/*
 * @package SPN
 * @author sheiko
 * @version 0.1
 * @license MIT
 * @copyright (c) Dmitry Sheiko http://www.dsheiko.com
 * Code style: http://docs.jquery.com/JQuery_Core_Style_Guidelines
 */

/*global window */

(function($) {
	var body = $('body');

	body.on('swiperight', function(e) {
		var URL = $('link[rel=prev]').first().attr('href');
		if (URL) {
			window.location = URL;
		}
	});

	body.on('swipeleft', function(e) {
		var URL = $('link[rel=next]').first().attr('href');
		if (URL) {
			window.location = URL;
		}
	});

	body.on('movestart', function(e) {
		// If the movestart is heading off in an upwards or downwards
		// direction, prevent it so that the browser scrolls normally.
		if ((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) {
			e.preventDefault();
		}
	});
})(jQuery);
